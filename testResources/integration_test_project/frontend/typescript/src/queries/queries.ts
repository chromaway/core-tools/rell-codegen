// Generated by typescript generator


import { QueryObject } from "postchain-client";
import { RawGtv } from "postchain-client";


export enum TestEnum {
	a
}


export enum MyNs1LocalTestEnum {
	a
}


export type TestStruct = {
	a: number;
};


export type MyNs1TestStruct2 = {
	name: string;
};


export type MyNs1LocalTestStruct = {
	a: number;
};


export type MyNs1MyNs12TestStruct2 = {
	a: number;
};


export function returnTypeEnumQueryObject(): QueryObject<string> {
	return { name: "return_type_enum" };
}


export function returnTypeBooleanQueryObject(): QueryObject<number> {
	return { name: "return_type_boolean" };
}


export function returnTypeIntegerQueryObject(): QueryObject<number> {
	return { name: "return_type_integer" };
}


export function returnTypeBigIntegerQueryObject(): QueryObject<bigint> {
	return { name: "return_type_big_integer" };
}


export function returnTypeTextQueryObject(): QueryObject<string> {
	return { name: "return_type_text" };
}


export function returnTypeDecimalQueryObject(): QueryObject<string> {
	return { name: "return_type_decimal" };
}


export function returnTypeByteArrayQueryObject(): QueryObject<Buffer> {
	return { name: "return_type_byte_array" };
}


export function returnTypePubkeyQueryObject(): QueryObject<Buffer> {
	return { name: "return_type_pubkey" };
}


export function returnTypeEntityQueryObject(): QueryObject<number> {
	return { name: "return_type_entity" };
}


export function returnTypeNullableEntityQueryObject(): QueryObject<number | null> {
	return { name: "return_type_nullable_entity" };
}


export function returnTypeStructQueryObject(): QueryObject<TestStruct> {
	return { name: "return_type_struct" };
}


export function returnTypeRowidQueryObject(): QueryObject<number> {
	return { name: "return_type_rowid" };
}


export function returnTypeNullableRowidQueryObject(): QueryObject<number | null> {
	return { name: "return_type_nullable_rowid" };
}


export function returnTypeGtvQueryObject(): QueryObject<RawGtv> {
	return { name: "return_type_gtv" };
}


export function returnTypeNullableGtvQueryObject(): QueryObject<RawGtv> {
	return { name: "return_type_nullable_gtv" };
}


export function returnTypeListIntegerQueryObject(): QueryObject<number[]> {
	return { name: "return_type_list_integer" };
}


export function returnTypeListBooleanQueryObject(): QueryObject<number[]> {
	return { name: "return_type_list_boolean" };
}


export function returnTypeListByteArrayQueryObject(): QueryObject<Buffer[]> {
	return { name: "return_type_list_byte_array" };
}


export function returnTypeSetIntegerQueryObject(): QueryObject<number[]> {
	return { name: "return_type_set_integer" };
}


export function returnTypeListStructQueryObject(): QueryObject<TestStruct[]> {
	return { name: "return_type_list_struct" };
}


export function returnTypeNullableListStructQueryObject(): QueryObject<TestStruct[] | null> {
	return { name: "return_type_nullable_list_struct" };
}


export function returnTypeListEntityQueryObject(): QueryObject<number[]> {
	return { name: "return_type_list_entity" };
}


export function returnTypeNullableListEntityQueryObject(): QueryObject<number[] | null> {
	return { name: "return_type_nullable_list_entity" };
}


export function returnTypeListGtvQueryObject(): QueryObject<RawGtv[]> {
	return { name: "return_type_list_gtv" };
}


export function returnTypeListListListQueryObject(): QueryObject<RawGtv[][][]> {
	return { name: "return_type_list_list_list" };
}


export function returnTypeSetGtvQueryObject(): QueryObject<RawGtv[]> {
	return { name: "return_type_set_gtv" };
}


export function returnTypeMapQueryObject(): QueryObject<Record<string, string>> {
	return { name: "return_type_map" };
}


export function returnTypeNullableMapQueryObject(): QueryObject<Record<string, string> | null> {
	return { name: "return_type_nullable_map" };
}


export function returnTypeEnumMapQueryObject(): QueryObject<Array<[TestEnum, string]>> {
	return { name: "return_type_enum_map" };
}


export function returnTypeNullableEnumMapQueryObject(): QueryObject<Array<[TestEnum, string]> | null> {
	return { name: "return_type_nullable_enum_map" };
}


export function returnTypeAnyMapQueryObject(): QueryObject<Array<[TestStruct, string]>> {
	return { name: "return_type_any_map" };
}


export type ReturnTypeNamedTupleReturnType = {
	foo: number;
};


export function returnTypeNamedTupleQueryObject(): QueryObject<ReturnTypeNamedTupleReturnType> {
	return { name: "return_type_named_tuple" };
}


export type ReturnTypeNullableNamedTupleReturnType = {
	foo: number;
};


export function returnTypeNullableNamedTupleQueryObject(): QueryObject<ReturnTypeNullableNamedTupleReturnType | null> {
	return { name: "return_type_nullable_named_tuple" };
}


export type ReturnTypeNamedTupleListReturnType = {
	rowid: number;
	a: number;
};

export function returnTypeNamedTupleListQueryObject(since: number): QueryObject<ReturnTypeNamedTupleListReturnType[]> {
	return { name: "return_type_named_tuple_list", args: { since: since } };
}


export function returnTypeUnnamedTupleQueryObject(): QueryObject<[number]> {
	return { name: "return_type_unnamed_tuple" };
}


export type ReturnTypeProposalsSinceReturnType = {
	rowid: number;
	a: number;
};

export function returnTypeProposalsSinceQueryObject(since: number): QueryObject<ReturnTypeProposalsSinceReturnType[]> {
	return { name: "return_type_proposals_since", args: { since: since } };
}


export function inputParameterNargsQueryObject(): QueryObject<number> {
	return { name: "input_parameter_nargs" };
}


export function inputParameterTextQueryObject(t: string): QueryObject<number> {
	return { name: "input_parameter_text", args: { t: t } };
}


export function inputParameterNullableQueryObject(t: string | null): QueryObject<number> {
	return { name: "input_parameter_nullable", args: { t: t } };
}


export function inputParameterEnumQueryObject(e: TestEnum): QueryObject<number> {
	return { name: "input_parameter_enum", args: { e: e } };
}


export function inputParameterIntegerQueryObject(i: number): QueryObject<number> {
	return { name: "input_parameter_integer", args: { i: i } };
}


export function inputParameterBigIntegerQueryObject(i: bigint): QueryObject<number> {
	return { name: "input_parameter_big_integer", args: { i: i } };
}


export function inputParameterBooleanQueryObject(b: number): QueryObject<number> {
	return { name: "input_parameter_boolean", args: { b: b } };
}


export function inputParameterRowidQueryObject(r: number): QueryObject<number> {
	return { name: "input_parameter_rowid", args: { r: r } };
}


export function inputParameterPubkeyQueryObject(pubkey: Buffer): QueryObject<number> {
	return { name: "input_parameter_pubkey", args: { pubkey: pubkey } };
}


export function inputParameterBlockchainRidQueryObject(blockchainRid: Buffer): QueryObject<number> {
	return { name: "input_parameter_blockchain_rid", args: { blockchain_rid: blockchainRid } };
}


export function inputParameterEntityQueryObject(e: number): QueryObject<number> {
	return { name: "input_parameter_entity", args: { e: e } };
}


export function inputParameterStructQueryObject(s: TestStruct): QueryObject<number> {
	return { name: "input_parameter_struct", args: { s: Object.values(s) } };
}


export function inputParameterListInputQueryObject(v: Buffer[]): QueryObject<number> {
	return { name: "input_parameter_list_input", args: { v: v } };
}


export function inputParameterNullableListInputQueryObject(v: Buffer[] | null): QueryObject<number> {
	return { name: "input_parameter_nullable_list_input", args: { v: v } };
}


export function inputParameterSetInputQueryObject(v: Set<Buffer>): QueryObject<number> {
	return { name: "input_parameter_set_input", args: { v: Array.from(v) } };
}


export function inputParameterMapInputQueryObject(v: Record<string, Buffer>): QueryObject<number> {
	return { name: "input_parameter_map_input", args: { v: v } };
}


export function inputParameterEnumMapQueryObject(m: Array<[TestEnum, Buffer]>): QueryObject<number> {
	return { name: "input_parameter_enum_map", args: { m: m } };
}


export function inputParameterAnyMapQueryObject(m: Array<[TestStruct, Buffer]>): QueryObject<number> {
	return { name: "input_parameter_any_map", args: { m: m } };
}


export function inputParameterMultipleQueryObject(s: string, s2: string): QueryObject<number> {
	return { name: "input_parameter_multiple", args: { s: s, s2: s2 } };
}


export function inputParameterGtvQueryObject(g: RawGtv): QueryObject<number> {
	return { name: "input_parameter_gtv", args: { g: g } };
}


export function inputParameterNullableGtvQueryObject(g: RawGtv): QueryObject<number> {
	return { name: "input_parameter_nullable_gtv", args: { g: g } };
}


export function myNs1Q1InNamespaceQueryObject(e: TestEnum): QueryObject<TestStruct> {
	return { name: "my_ns1.q1_in_namespace", args: { e: e } };
}


export function myNs1Q2InNamespaceQueryObject(s: MyNs1TestStruct2): QueryObject<MyNs1TestStruct2> {
	return { name: "my_ns1.q2_in_namespace", args: { s: Object.values(s) } };
}


export function myNs1Q3aReturnTypeEnumQueryObject(e: MyNs1LocalTestEnum): QueryObject<string> {
	return { name: "my_ns1.q3a_return_type_enum", args: { e: e } };
}


export function myNs1Q3bReturnTypeEnumQueryObject(m: Array<[TestEnum, Buffer]>): QueryObject<string> {
	return { name: "my_ns1.q3b_return_type_enum", args: { m: m } };
}


export function myNs1Q4ReturnTypeListStructQueryObject(m: Array<[Buffer, MyNs1MyNs12TestStruct2]>): QueryObject<TestStruct[]> {
	return { name: "my_ns1.q4_return_type_list_struct", args: { m: m } };
}


export function myNs1Q5ReturnTypeListStructQueryObject(v: MyNs1LocalTestStruct[]): QueryObject<MyNs1LocalTestStruct[]> {
	return { name: "my_ns1.q5_return_type_list_struct", args: { v: v } };
}


export function myNs1Q6ReturnTypeListStructQueryObject(): QueryObject<MyNs1MyNs12TestStruct2[]> {
	return { name: "my_ns1.q6_return_type_list_struct" };
}


export function myNs1Q7ReturnTypeEnumMapQueryObject(): QueryObject<Array<[TestEnum, string]>> {
	return { name: "my_ns1.q7_return_type_enum_map" };
}


export function myNs1Q8ReturnTypeEnumMapQueryObject(): QueryObject<Array<[MyNs1LocalTestEnum, string]>> {
	return { name: "my_ns1.q8_return_type_enum_map" };
}


export function myNs1Q9ReturnTypeAnyMapQueryObject(): QueryObject<Array<[TestStruct, string]>> {
	return { name: "my_ns1.q9_return_type_any_map" };
}


export function myNs1Q10ReturnTypeAnyMapQueryObject(): QueryObject<Array<[MyNs1LocalTestStruct, string]>> {
	return { name: "my_ns1.q10_return_type_any_map" };
}


export function myNs1MyNs2Q2InNamespaceQueryObject(): QueryObject<string> {
	return { name: "my_ns1.my_ns2.q2_in_namespace" };
}


export type MyNs1MyNs2Q3InNamespaceReturnType = {
	foo: number;
};

export function myNs1MyNs2Q3InNamespaceQueryObject(): QueryObject<MyNs1MyNs2Q3InNamespaceReturnType> {
	return { name: "my_ns1.my_ns2.q_3_in_namespace" };
}
