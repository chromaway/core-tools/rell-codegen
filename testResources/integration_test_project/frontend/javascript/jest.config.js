
export default {
    testEnvironment: 'jest-environment-node',
    transform: {}, // Explicitly state no transformation
    testMatch: ['**/tests/**/*.test.js'],
    moduleFileExtensions: ['js'],
};