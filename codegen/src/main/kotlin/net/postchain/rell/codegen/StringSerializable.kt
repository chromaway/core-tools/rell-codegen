package net.postchain.rell.codegen

fun interface StringSerializable {
    fun format(): String
}