package net.postchain.rell.codegen

enum class FileSaveMode {
    Dapp,
    Module,
    Separate,
}
