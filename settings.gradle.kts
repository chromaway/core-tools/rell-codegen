
rootProject.name = "rell-codegen"
include("rellgen")
include("codegen")
include("codegen-kotlin")
include("codegen-typescript")
include("codegen-javascript")
include("codegen-mermaid")
