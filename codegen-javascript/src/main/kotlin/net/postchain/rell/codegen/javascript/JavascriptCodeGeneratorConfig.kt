package net.postchain.rell.codegen.javascript

import net.postchain.rell.codegen.CodeGeneratorConfig

interface JavascriptCodeGeneratorConfig: CodeGeneratorConfig
