package net.postchain.rell.codegen.typescript

import net.postchain.rell.codegen.CodeGeneratorConfig

interface TypescriptCodeGeneratorConfig: CodeGeneratorConfig
