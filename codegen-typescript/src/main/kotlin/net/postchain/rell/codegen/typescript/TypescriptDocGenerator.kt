package net.postchain.rell.codegen.typescript

import net.postchain.rell.codegen.docs.AbstractDocGenerator

object TypescriptDocGenerator : AbstractDocGenerator()
