package net.postchain.rell.codegen.app.util

enum class LanguageSupport {
    Kotlin,
    Typescript,
    Javascript,
    Mermaid
}
